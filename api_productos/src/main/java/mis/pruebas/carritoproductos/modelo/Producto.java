package mis.pruebas.carritoproductos.modelo;

import java.util.ArrayList;
import java.util.List;

// POJO - Plain Old Java Objects
public class Producto {
    long id;
    String nombre;
    double precio;
    double cantidad;
    final List<Integer> idsProveedores = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Producto(long id, String nombre, double precio, double cantidad) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public List<Integer> getIdsProveedores() {
        return idsProveedores;
    }
}
