package mis.pruebas.entregable1.controller;

import mis.pruebas.entregable1.models.ProductoModel;
import mis.pruebas.entregable1.models.ProductoPrecio;
import mis.pruebas.entregable1.servicio.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("")
    public String root() {
        return "Entregable 1";
    }

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productService.getProductos();
    }

    @GetMapping("/productos/size")
    public int countProductos() {
        return productService.getProductos().size();
    }

    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        ProductoModel pm = productService.getProducto(id);
        if (pm == null) {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pm);
    }

    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel prodM) {
        productService.addProducto(prodM);
        return new ResponseEntity<>("Producto creado", HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody ProductoModel update) {
        ProductoModel pm = productService.getProducto(id);
        if (pm == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.updateProducto(id - 1, update);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        ProductoModel pm = productService.getProducto(id);
        if (pm == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.removeProducto(id - 1);
        return new ResponseEntity<>("Producto eliminado correctamente.", HttpStatus.OK);
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(@RequestBody ProductoPrecio productoPrecio,
                                              @PathVariable int id) {
        ProductoModel pm = productService.getProducto(id);
        if (pm == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pm.setPrecio(productoPrecio.getPrecio());
        productService.updateProducto(id - 1, pm);
        return new ResponseEntity<>(pm, HttpStatus.OK);
    }

    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id) {
        ProductoModel pm = productService.getProducto(id);
        if (pm == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pm.getUsers() != null)
            return ResponseEntity.ok(pm.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
