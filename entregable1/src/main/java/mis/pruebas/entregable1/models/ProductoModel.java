package mis.pruebas.entregable1.models;

import java.util.List;
import java.util.Objects;

public class ProductoModel {
    private long id;
    private String desc;
    private double precio;
    private List<UserModel> users;

    public ProductoModel() {

    }

    public ProductoModel(long id, String desc, double precio) {
        this.id = id;
        this.desc = desc;
        this.precio = precio;
    }

    public ProductoModel(long id, String desc, double precio, List<UserModel> users) {
        this.id = id;
        this.desc = desc;
        this.precio = precio;
        this.users = users;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductoModel{");
        sb.append("id=").append(id);
        sb.append(", descripcion='").append(desc).append('\'');
        sb.append(", precio=").append(precio);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductoModel)) return false;
        ProductoModel producto = (ProductoModel) o;
        return getId() == producto.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
