package mis.pruebas.entregable1.servicio;

import mis.pruebas.entregable1.models.ProductoModel;
import mis.pruebas.entregable1.models.UserModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductService {
    private List<ProductoModel> datalist = new ArrayList<ProductoModel>();

    public ProductService() {
        datalist.add(new ProductoModel(1, "producto 1", 100));
        datalist.add(new ProductoModel(2, "producto 2", 124.50));
        datalist.add(new ProductoModel(3, "producto 3", 149.50));
        datalist.add(new ProductoModel(4, "producto 4", 200.00));
        datalist.add(new ProductoModel(5, "producto 5", 249.50));
        List<UserModel> users = new ArrayList<>();
        users.add(new UserModel("1"));
        users.add(new UserModel("3"));
        users.add(new UserModel("5"));
        datalist.get(1).setUsers(users);
    }

    //Collection
    public List<ProductoModel> getProductos() {
        return datalist;
    }

    //Instance
    public ProductoModel getProducto(long index) throws IndexOutOfBoundsException {
        if (getIndex(index) >= 0) {
            return datalist.get(getIndex(index));
        }
        return null;
    }

    //Create
    public ProductoModel addProducto(ProductoModel newProd) {
        datalist.add(newProd);
        return newProd;
    }

    //update
    public ProductoModel updateProducto(int index, ProductoModel newProd) throws IndexOutOfBoundsException {
        datalist.set(index, newProd);
        return datalist.get(index);
    }

    //Delete
    public void removeProducto(int index) throws IndexOutOfBoundsException {
        int pos = datalist.indexOf(datalist.get(index));
        datalist.remove(pos);
    }

    //Index
    public int getIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<datalist.size()) {
            if(datalist.get(i).getId() == index){
                return(i); }
            i++;
        }
        return -1;
    }
}
