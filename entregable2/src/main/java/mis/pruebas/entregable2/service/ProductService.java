package mis.pruebas.entregable2.service;

import mis.pruebas.entregable2.models.ProductoModel;
import mis.pruebas.entregable2.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String id) {
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel entity) {
        return productoRepository.save(entity);
    }

    public boolean delete(ProductoModel entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
