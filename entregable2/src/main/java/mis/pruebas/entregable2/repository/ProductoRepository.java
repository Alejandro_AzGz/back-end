package mis.pruebas.entregable2.repository;

import mis.pruebas.entregable2.models.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository  extends MongoRepository<ProductoModel, String> {
}
