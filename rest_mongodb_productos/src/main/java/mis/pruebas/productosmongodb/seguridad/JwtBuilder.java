package mis.pruebas.productosmongodb.seguridad;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.ErrorCodes;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class JwtBuilder {

    @Value("${autenticacion.emisor}")
    private String emisor;

    @Value("${autenticacion.audiencia}")
    private String audiencia;

    @Value("${autenticacion.asunto}")
    private String asunto;

    @Value("${autenticacion.secreto}")
    private String secreto;

    @Value("${autenticacion.expiracion}")
    private float expiracion;

    RsaJsonWebKey rsaJsonWebKey;

    public JwtBuilder() {
    }

    public String generarToken(String usuario) {
        // encabezado.carga-util(declaraciones).firma

        // "claims"
        final JwtClaims declaraciones = new JwtClaims();
        declaraciones.setIssuer(this.emisor);
        declaraciones.setExpirationTimeMinutesInTheFuture(this.expiracion);
        declaraciones.setIssuedAtToNow();
        declaraciones.setAudience(this.audiencia);
        declaraciones.setSubject(this.asunto);
        declaraciones.setClaim("usuario", usuario);
        declaraciones.setGeneratedJwtId();

        final JsonWebSignature firma = new JsonWebSignature();
        firma.setPayload(declaraciones.toJson());
        firma.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
        firma.setKey(this.rsaJsonWebKey.getPrivateKey());

        try {
            return firma.getCompactSerialization();
        } catch (Exception x) {
            throw new RuntimeException("No se pudo generar el token");
        }
    }

    public JwtClaims validarToken(String token) {

        final JwtConsumer consumidor = new JwtConsumerBuilder()
                // Le digo que estoy esperando del token.
                // Los mismos parámetros que usó el servidor de autenticación para generar el token.
                .setRequireExpirationTime()
                .setSkipSignatureVerification()
                .setAllowedClockSkewInSeconds(60)
                .setRequireSubject()
                .setExpectedAudience(this.audiencia)
                .setExpectedSubject(this.asunto)
                .setExpectedIssuer(this.emisor)
                .setVerificationKey(this.rsaJsonWebKey.getKey())
                // IMPORTANTISIMO!!!
                .setJwsAlgorithmConstraints(new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST,
                        AlgorithmIdentifiers.RSA_USING_SHA256))
                .build();

        try {
            final JwtClaims declaraciones = consumidor.processToClaims(token);
            return declaraciones;
        } catch (InvalidJwtException x) {
            if (x.hasExpired())
                throw new RuntimeException("El token ha expirado.");
            if (x.hasErrorCode(ErrorCodes.AUDIENCE_INVALID))
                throw new RuntimeException("No es la audiencia correcta para este servicio.");

            // Respuesta genérica.
            throw new RuntimeException("El token no es válido.");
        }
    }


    @PostConstruct
    public void inicializar() {
        try {
            this.rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
            this.rsaJsonWebKey.setKeyId(this.secreto);
        } catch (JoseException x) {
            x.printStackTrace();
        }
    }
}
