package mis.pruebas.borramespring.controlador;

import org.springframework.web.bind.annotation.*;

@RestController
public class ControladorHolaMundo {

    private String saludo = "Hola";

    //@RequestMapping(method = RequestMethod.GET, path = "/api-hola/v1/saludos")
    @GetMapping("/api-hola/v1/saludos")
    public String saludar(@RequestParam(name = "saludar", required = false) String nombre) {
        if (nombre == null || nombre.isEmpty())
            nombre = "Mundo";
        if (saludo == null || saludo.isEmpty())
            saludo = "Hola";
        return String.format("¡%s, %s!", saludo, nombre);
    }

    @PostMapping("/api-hola/v1/saludos")
    public void agregarSaludo(@RequestBody String saludo) {
        this.saludo = saludo;
    }

    @DeleteMapping("/api-hola/v1/saludos")
    public void borrarSaludo() {
        this.saludo = "Hola";
    }
}
